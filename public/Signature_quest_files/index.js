const address = config.apiUrl;
"use strict", Vue.use(VuejsDialog.main.default), Vue.prototype.$http = axios, Vue.use(VueI18n);
const router = new VueRouter({
    routes: []
  }),
  userLang = navigator.language || navigator.userLanguage;
moment.locale(userLang), console.log(userLang);
const i18n = new VueI18n({
    locale: userLang,
    messages: {
      en: en,
      fr: fr
    },
    fallbackLocale: "en"
  }),
  myApp = new Vue({
    i18n: i18n,
    router: router,
    el: "#main",
    data: {
      courseId: "",
      studentId: "",
      verificationToken: "",
      course: {
        ID: "",
        NAME: "",
        START: "",
        END: "",
        ATTENDANCE_LIST_GENERATED: "",
        LOCKED: !1,
        EMAIL_STUDENT_ASSOCIATED: {
          FIRSTNAME: "",
          LASTNAME: "",
          EMAIL: ""
        },
        SCHOOL: {
          LOGO: "",
          NAME: ""
        }
      },
      signaturePad: "",
      errorText: "",
      formattedStart: "",
      formattedEnd: "",
      hasBeenSigned: !1,
      alreadySigned: !1,
      isLoading: !1
    },
    mounted: function() {
      var a = this;
      let b = this.$route.query;
      var c = document.querySelector("canvas");
      a.signaturePad = new SignaturePad(c, {
        minWidth: 1,
        maxWidth: 3,
        penColor: "black"
      });
      var d = 400 < window.screen.width ? 400 : window.screen.width;
      c.width = d, c.height = 200
    },
    methods: {
      clearSignPad: function() {
        console.log("clear sign pad"), this.signaturePad.clear()
      },
      saveSignature: function() {
        var a = this;
        console.log("validate signature");
        var b = this.signaturePad.toDataURL()
        const link = document.createElement("a");
        link.href=b;
        link.download = "signature";
        link.click();
        URL.revokeObjectURL(link.href);
        this.signaturePad.clear();
      }
    }
  });
